South East Queenslands Air Conditioning Specialists - 
We can design, supply and install air conditioning to meet your requirements utilising a fully ducted, wall mounted split or a room air conditioner system. We have installed air conditioning in major retail outlets, shopping complexes, fitness centres, educational institutions and medical facilities. Regardless of whether it is a large commercial property, a home or a unique application installation we can help.

Website: https://thompsoncooling.com.au/
